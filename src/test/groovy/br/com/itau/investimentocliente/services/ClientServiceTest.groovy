package br.com.itau.investimentocliente.services

import br.com.itau.investimentocliente.models.Cliente
import br.com.itau.investimentocliente.repositories.ClienteRepository
import spock.lang.Specification

class ClientServiceTest  extends Specification{
	
	ClienteService clienteService
	ClienteRepository clienteRepository
	
	def setup() {
		clienteService = new ClienteService()
		clienteRepository = Mock()
		
		clienteService.clienteRepository = clienteRepository
	}
	

	def 'deve salvar um cliente'() {
		given: 'os dados de um cliente sejam informados'
		Cliente cliente = new Cliente()
		cliente.setNome("Felipe")
		cliente.setCpf("123.123.123.12")
		
		when: 'o cliente é salvo'
		def clienteSalvo = clienteService.cadastrar(cliente)
		
		then: 'retorne um cliente'
		1 * clienteRepository.save(_) >> cliente
		clienteSalvo != null
	}
	
	def 'deve buscar um cliente por CPF'(){
		given: 'os dados de um cliente exista na base'
		Cliente cliente = new Cliente()
		cliente.setNome("Felipe")
		String cpf = "123.123.123.-12"
		cliente.setCpf(cpf)
		
		def clienteOptional = Optional.of(cliente)
		
		when: 'é feita uma busca na base de dados'
		def clienteEncontrado = clienteService.buscar(cpf)
		
		then: 'retorne os dados do cliente do cpf informado'
		1 * clienteRepository.findByCpf(_) >> clienteOptional
		clienteEncontrado.isPresent() == true
		
	}
	

	
}
